## Rechercher les lieux, les lieux de la recherche

*Enjeux et pratiques d'identification et d'indexation de l'information géographique dans les textes scientifiques*

Le 18 juin 2019, à Olympe de Gouges, 8 Rue Albert Einstein, 75013 Paris de 9h15 à 17h00  
- - -  

- 9h15-9h30 *Accueil et mots d'introduction*

#### Interroger les pratiques et le rapport aux terrains des chercheurs

- 9h30-10h10  
    Terrains surpâturés, science à distance, et mots-clefs de la géographie : tour d'horizon des travaux de l'UMR Passages  
    *Grégoire Lecampion, Marine Duc et Matthieu Noucher*, Laboratoire PASSAGES, Bordeaux
- 10h10-10h40  
    Le versant français des Pyrénées, terrain de la science mondiale ?  
    *Marion Maisonobe*, Laboratoire Géographie-cités, Paris

- 10h40-10h50 *Pause*

#### Méthodes pour identifier les terrains d'études dans les corpus scientifiques

- *10h50-11h30*  
    Accès à l’information géographique dans les textes  
    *Christian Sallaberry*, Laboratoire LIUPPA, Pau

- *11h30-12h10*  
    L'identification d'entités géographiques nommées avec la plateforme Cortext  
    *Lionel Villard*, Laboratoire LISIS, Marne-la-Vallée

- *12h10-13h30 - Déjeuner*  

#### Enjeux et propositions méthodologiques pour la recherche d'information

- *13h30-13h55*  
    Désambiguïser les entités géographiques dans les publications de la base ISTEX  
    *Pascal Cuxac*, INIST, Vandœuvre-lès-Nancy

- *13h55-14h20*  
    Entités nommées et indexation géographique dans la recherche d'information    
    *Mathieu Orban*, OpenEdition, Marseille

- *14h20-14h50 - Discussion animée par Guillaume Cabanac, Laboratoire IRIT, Toulouse*

#### Le projet PACTOLS en archéologie et *CybergeoNetworks* en géographie

- *15h00-15h30*  
    Le projet d'intégration de PACTOLS pour les revues d'OpenEdition  
    *Mélanie Carmona*, OpenEdition, Marseille et *Edith Cannet*, Métopes, Caen

- *15h30-16h00*  
    Le projet *CybergeoNetworks* pour les revues de géographie  
    *Clémentine Cottineau*, Centre Maurice Halbwachs, Paris et *Juste Raimbault*, ISCPIF, Paris

#### Réflexions sur les pratiques d'indexation des lieux dans les revues de géographie

- *16h00-17h00*  
    Table ronde autour des pratiques d'indexation en géographie avec *Christine Kosmopoulous* (Cybergeo), 
    *Laurent Jégou* (M@ppemonde), *Clarisse Didelon* (M@ppemonde), *Hervé Théry* (Confins) et *Matthieu Noucher*.
    Animée par *Guillaume Cabanac* et *Marion Maisonobe*



    

